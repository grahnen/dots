#!/bin/env python
from enum import Enum
import sys
import os
from pathlib import Path
import glob
import subprocess as s
class Mode(Enum):
    laptop='laptop'
    desktop='desktop'

    def __str__(self):
        return self.value

def patch_keyboard():
    cmd = "patch < keyboard.patch"
    
    p = s.check_output(cmd, shell=True)

def setup(mode):
    file="dots.org"
    cmd=' '.join([
        "/usr/bin/emacs -Q --batch --eval \"",
        "(progn ",
        " (require \'ob-tangle) ",
        " (setq org-use-property-inheritance t",
        "      org-confirm-babel-evaluate nil)",
        " (org-babel-do-load-languages ",
        " 'org-babel-load-languages ",
        "'((shell . t) ",
        "(emacs-lisp . t))) ",
        f" (let ((option '{mode}))",
        f'  (org-babel-tangle-file \\""{file}\\"")))"'
    ])

    p = s.check_output(cmd, shell=True)

def link_files():
    home = os.getenv('HOME')
    for path in Path('tangled').glob('**/*'):
        newpath = home / path.relative_to('tangled')
        print(newpath)
        if path.is_dir():
            if newpath.exists():
                if newpath.is_dir():
                    print('Folder ', newpath, ' exists.')
                else:
                    print('Moving ', newpath, ' to ', newpath, '.bak')
                    newpath.rename(newpath.with_suffix(newpath.suffix + '.bak'))
            else:
                print('Creating directory ', newpath)
                newpath.mkdir(parents=True)
        else:
            if newpath.exists():
                if newpath.is_symlink():
                    if newpath.resolve() == path:
                        print(newpath, ' already linked.')
                    else:
                        print(newpath, ' target replaced with ', path)
                        newpath.unlink()
                        newpath.symlink_to(Path.cwd() / path)
                else:
                    print(newpath, ' renamed to ', newpath, '.bak')
                    newpath.rename(newpath.with_suffix(path.suffix + '.bak'))
            else:
                if newpath.is_symlink():
                    print(newpath, 'is a broken link')
                    newpath.unlink()
                print(newpath, ' linked to ', path)
                newpath.symlink_to(Path.cwd() / path)
if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Setup and install configuration')
    parser.add_argument('mode', type=Mode, choices=list(Mode), help='The mode to tangle')
    parser.add_argument('-t',action='store_false', help='Tangle only, don\'t link')
    parser.add_argument('-p',action='store_true', help='Patch Keyboard Config')
    args = parser.parse_args()

    if (args.p):
        patch_keyboard()
    else:
        setup(args.mode)
        if(args.t):
            link_files()
        
